---
sidebar_position: 1
---

# JS SDK

## 安装

```bash
npm i osp-client-js@latest --save-dev
```


## Auth
### 登录--signIn
signIn用户在获取用户钱包地址后，登录DAPP
```typescript
const res = await client.auth.signIn()
```
#### 案例
```typescript
import React from 'react'
import { Client } from 'osp-client-js'

export function Auth({client}:{ client: Client }){

  const handleSignIn = async()=>{
    const res = await client.auth.signIn()
    console.log('res', res)
  }

  return (
    <>  
        <h2>Auth模块</h2>
        <h3>登录</h3>

        <button onClick={handleSignIn}>signIn</button>
    </>
  )
}
```
#### 参数

无

#### 返回

```typescript
{
  err: Error,
  data: {
    accessToken: string,
    refreshToken: string
  }
}
```


## Porfile
### 创建用户的 Profile
账户的所有操作都是基于创建的profile，所以在进行其他操作前，需要先创建profile
```typescript
```typescript
const res = await client.user.create(...)
```
#### 案例
```typescript
import React from 'react'
import { Client } from 'osp-client-js'

// 一个需要付费关注的 mock 数据
const mock = {
   // 用户关注时候是何种方式关注 
    "follow_module":{
        /***
         * 付费关注(当type不为付费关注时候init为null)
         * type: FollowModuleEnum
         * 免费关注
         * FeeFollowModule
         * 用户必须有profile才能关注
         * ProfileFollowModule
         * NULL
         * NullFollowModule
         * 
        */
        "type":"FeeFollowModule",
        "init":{
          // 付费关注
            "FeeFollowModule":{
                "amount":{
                  //  代币地址（用那种代币支付）
                    "currency":"0x8A791620dd6260079BF849Dc5567aDC3F2FdC318",
                    // 数量
                    "value":"1"
                },
                // 接收账户
                "recipient":"0x15d34AAf54267DB7D7c367839AAf71A00a2C6A65"
            }
        }
    },
  //  用户名（必须为字母与数字的组合）
    "handle":"aaaa11",
  //  简介
    "bio":"1",
    // 头像
    "avatar":"1",
    // 封面
    "cover_picture":"1"
} 

export function Create({client}:{ client: Client }){


  const createProfiles = async()=>{
    const { follow_module,follow_nft_currency,follow_nft_amount,follow_nft_recived, ...rest } = form
    const res = await client.user.create(mock)
    console.log('res', res)
  }

  return (
    <>  
        <p>注意：头像为必传， 用户名不能少于5位,创建profile需要先登陆</p>
        <button onClick={createProfiles}>创建用户的profile</button>
    </>
  )
}

```
#### 返回

```typescript
{
  err: Error,
  data: {
    obj: {
      /** txHash if success */
      tx_hash?: string;
      /** txId if indexed by relay */
      tx_id?: string;
      info?: object;
    }
  }
}
```  
### 获取用户所有的的 Profile
```typescript
import React from 'react'
import { Client } from 'osp-client-js'
client.user.getAllProfile()
```
#### 返回

```typescript
{
  err: Error,
  data: {
    obj?: {
      /**
       * @format int32
       * @min 1
       * @max 20
       */
      limit?: number;
      /** @format int32 */
      total?: number;
      next_token?: string;
      rows?: User[];
    }
  }
}
```
### 获取用户所有的的 Profile详情
```typescript
import React from 'react'
import { Client } from 'osp-client-js'
// profileId 为User的profile_id
const profileId = '0xf7'
client.user.get(profileId)
```
#### 返回

```typescript
{
  err: Error,
  data: {
    obj?: User;
  }
}
```
## Activity

#### mock数据  <a name="activityMockData"></a>
``` js
const metadata: any = {
    "version":"2.0.0",
    "metadata_id":"c58be35b-b036-41ff-810d-959d08685ea8",
    "content":"hihi",
    "external_url":"https://lenster.xyz/u/diwucun.test"
    ,"image": null,
    "imageMimeType":null,
    "name":"testpostdy",
    "tags":[],
    "animation_url":null,
    "mainContentFocus":
    "TEXT_ONLY",
    "contentWarning":null,
    "attributes":[{"traitType":"type","displayType":"string","value":"text_only"}],
    "media":[],
    "locale":"zh-CN",
    "appId":"Lenster"
}

const collect = {
    "type": CollectModuleEnum.FreeCollectModule,
    "init": {
      "FreeCollectModule": {
        "only_follower": false
      }
    }
}

const reference =  {
    "type": ReferenceModuleEnum.FollowerOnlyReferenceModule,
    "init": {}
}
```
### 创建帖子Activity--addActivity
用户登录后，在拥有profile后，创建不上链的Activity
```js
client.activity.addActivity({
				profileId,
				metadata,
				collect,
				reference
})
```
#### 案例
mock：[获取mock数据](#activityMockData)
```typescript
import React from 'react'
import { Client } from 'osp-client-js'

// metadata,collect,reference 与上面案例一致

export function createActivity({client}:{ client: Client }){

    const createActivity = async()=>{
			await client.activity.addActivity({
        profile_id: profileId,
        content_uri,
        collect_module: collect,
        reference_module: reference
      },{ headers:{ 'On-Chain': `${isOnChain}` }})
    }

  return (
    <>  
        <button onClick={createActivity}>创建Acvitity</button>
    </>
  )
}

```
#### 返回

```typescript
{
  err: Error,
  data: {
    obj:{
      /** txHash if success */
      tx_hash?: string;
      /** txId if indexed by relay */
      tx_id?: string;
      info?: object;
    }
  }
}
```  



### 创建帖子Activity--addActivityBroadCast
用户登录后，在拥有profile后，通过广播的形式创建activity  
```js
client.activity.addActivityBroadCast({
				profileId,
				metadata,
				collect,
				reference
})
```
#### 案例  
mock：[获取mock数据](#activityMockData)
```typescript
import React from 'react'
import { Client } from 'osp-client-js'

// metadata,collect,reference 与上面案例一致

export function addActivityBroadCast({client}:{ client: Client }){

    const addActivityBroadCast = async()=>{
			await client.activity.addActivityBroadCast({
            profileId,
            metadata,
            collect,
            reference
      })
    }

  return (
    <>  
        <button onClick={addActivityBroadCast}>创建Acvitity</button>
    </>
  )
}

```
#### 返回

```typescript
{
  err: Error,
  data: {
      obj:{
        tx_hash: string;
      }
  }
}
```  



### 创建帖子Activity--createActivityPostWithSig
通过调用合约的PostWithSig方法创建帖子Activity
这种调用合约的方式能显示签名信息
```js
client.activity.createActivityPostWithSig({
				profileId,
				metadata,
				collect,
				reference
})
```
#### 案例  
mock：[获取mock数据](#activityMockData)
```typescript
import React from 'react'
import { Client } from 'osp-client-js'

// metadata,collect,reference 使用与上面例子项目的数据即可


export function createActivityPostWithSig({client}:{ client: Client }){

    const createActivityPostWithSig = async()=>{
			await client.activity.createActivityPostWithSig({
        profile_id: profileId,
        content_uri,
        collect_module: collect,
        reference_module: reference
      })
    }

  return (
    <>  
        <button onClick={createActivityPostWithSig}>创建Acvitity</button>
    </>
  )
}

```
#### 返回

```typescript
{
  err: Error | ''
}
```  
### 创建帖子Activity--createActivityPost
通过调用合约的post方法创建帖子Activity
这种调用合约的方式无法不显示签名信息
```js
client.activity.createActivityPost({
				profileId,
				metadata,
				collect,
				reference
})
```
#### 案例  

mock：[获取mock数据](#activityMockData)
```typescript
import React from 'react'
import { Client } from 'osp-client-js'

// metadata,collect,reference 使用与上面例子项目的数据即可


export function createActivityPost({client}:{ client: Client }){

    const createActivityPost = async()=>{
			await client.activity.createActivityPost({
        profile_id: profileId,
        content_uri,
        collect_module: collect,
        reference_module: reference
      })
    }

  return (
    <>  
        <button onClick={createActivityPost}>创建Acvitity</button>
    </>
  )
}

```
#### 返回

```typescript
{
  err: Error | ''
}
```  



## Demo

https://osp-client.vercel.app/


## 注意


### 如何获取钱包地址
建议您使用[wagmi](https://wagmi.sh)中的示例方法获取钱包地址。



