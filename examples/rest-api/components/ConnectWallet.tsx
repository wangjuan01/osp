import React from 'react'

import { useConnect, useDisconnect, useAccount } from 'wagmi';
import { InjectedConnector } from 'wagmi/connectors/injected'

export function ConnectWallet({children, ...rest}) {
  const { connect } = useConnect({
    connector: new InjectedConnector(),
  })
  const { address, isConnected } = useAccount()

 
  if(isConnected) {
    if(Array.isArray(children)){
      return (
        children.map((child) => {
          return React.cloneElement(child, {...rest, ...{address}})
        })
        )
      }
      return React.cloneElement(children, {...rest, ...{address}})
  }

  return <button onClick={() => connect()}>Connect Wallet</button>
}
  