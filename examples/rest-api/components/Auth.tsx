import React from 'react';
import { useState } from 'react'

import {  useSignMessage } from 'wagmi';

export function Auth({address, apiClient}:{address?: string, apiClient?: any}){

// export function Auth({address, apiClient}){
  const [challenage, setChallenage] = useState('')
  const [accessToken, setAccessToken] = useState('')
  const [refreshToken, setRefreshToken] = useState('')
  const { signMessageAsync } = useSignMessage({ onError:(error: any)=>console.log('error',error) });
  const [accessTokenVerifyInfo, setAccessTokenVerifyInfo] = useState('')
  const [refreshTokenVerifyInfo, setRefreshTokenVerifyInfoo] = useState('')

  const handleFetchChallenge = async()=>{
    const {error, data} = await apiClient.auth.getChallenge({address}, {})
    console.log("ret", data)
    if(!error){
      setChallenage(data?.obj?.text)
    }
  }


  const handleSignIn = async()=>{
    // Get signature
    const signature = await signMessageAsync({
      message: challenage
    });



    // // Auth user and set cookies
    const {error, data}: any  = await apiClient.auth.authenticate({ address, signature } );
    console.log('auth', data, error)
    localStorage.setItem('accessToken', data?.obj?.access_token);
    localStorage.setItem('refreshToken', data?.obj?.refresh_token);
    setAccessToken(data?.obj?.access_token)
    setRefreshToken(data?.obj?.refresh_token)
    apiClient.setSecurityData({
      headers: {
        Authorization: `Bearer ${data?.obj?.access_token}`
      }
    })
    


  }

  const handleAuthVerify = async()=>{
    // accessToken verify info
    const {err, data}: any  = await apiClient.auth.verify({ access_token: accessToken } );
    console.log('accessToken verify info', data, err)
    setAccessTokenVerifyInfo(data?.obj?.access_token)

  }

  const handleAuthRefresh = async()=>{
    // accessToken verify info
    const {err, data}: any  = await apiClient.auth.refreshToken({ refresh_token:refreshToken } );
    console.log('refreshToken verify info', data, err)
    setRefreshTokenVerifyInfoo(data.obj.refresh_token)

  }

  return (
    <>  
        <h2>Auth模块</h2>
        <h3>获取Challenge</h3>
        <button onClick={handleFetchChallenge}>loadChallenge</button>
        {
          challenage
          ?
          <p>
            <span>{challenage}</span>

            <h3>登录</h3>

            <button onClick={handleSignIn}>signIn</button>
            {
              accessToken
              ?
              <>
                <p>
                  <h3>验证AccessToken</h3>
                  <button onClick={handleAuthVerify}>authVerify</button>
                </p>

                <p>
                  <h3>获取refreshToken</h3>
                  <span>refreshToken: {refreshToken}</span>
                  <br/>
                  <button onClick={handleAuthRefresh}>authRefresh</button>
                </p>
              </>
              :
              null
            }
          </p>
        :
        null
      }
    </>
  )
}