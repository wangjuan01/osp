import React from 'react';
import { useState } from 'react'

export const Reaction = ({address, apiClient}:{address?: string, apiClient?: any})=>{

  const [commentsList, setCommentsList] = useState([])


  const handleGetCommentsList = async () => {
    const lookupAttr = 'ACTIVITY';
    const lookupValue = 'QU5TV0VSOkFOU1dFUl80NDg5NzY2MTU4NTQ4OTkyMDoxNjgzMjc4MjI4MzgxMDAw';
    const kind = 'COMMENT';
    const query = {
      kind: 'COMMENT',
      ranking: 'TIME',
      limit: 10,
      // next_token
      with_activity: false
    };
    const {err, data}: any  = await apiClient.reaction.getReactions(lookupAttr, lookupValue, kind, query);
    setCommentsList(data?.obj?.rows || [])
    console.log('handleGetFollowingList info', data, err)
  }

  const handleDoFavorite = async () => {
    const queryBody = {
      user_id: '44895703185260544',
      target_user_id: '48811062648930304',
      target_activity_id: 'UE9TVDpQT1NUXzQ4ODExMDYyNjQ4OTMwMzA0OjE2ODQxNDA4MTYyMTMwMDA=',
      kind: 'FAVORITE',
    }
    const params = {
      headers: {
        'On-Chain': false,
      }
    }
    const {err, data}: any  = await apiClient.reaction.addReaction(queryBody, params);
    console.log('handleGetFollowingList info', data, err)
  }

  const handleDoComment = async () => {
    const queryBody = {
      user_id: '44895703185260544',
      target_user_id: '48811062648930304',
      target_activity_id: 'UE9TVDpQT1NUXzQ4ODExMDYyNjQ4OTMwMzA0OjE2ODQxNDA4MTYyMTMwMDA=',
      content_uri:'ipfs://QmYqrhL8Nzec4swV6bsHsv1ioojJUgRerszRsfeGz8VhhN',
      kind: 'COMMENT',
    }
    const {err, data}: any  = await apiClient.reaction.addReaction(queryBody);
    console.log('handleGetFollowingList info', data, err)
  }

  const handleDoReply = async () => {
    const queryBody = {
      // user_id: '44895703185260544',
      // target_user_id: '48811062648930304',
      // target_activity_id: 'UE9TVDpQT1NUXzQ4ODExMDYyNjQ4OTMwMzA0OjE2ODQxNDA4MTYyMTMwMDA=',
      content_uri:'ipfs://QmYqrhL8Nzec4swV6bsHsv1ioojJUgRerszRsfeGz8VhhN',
      kind: 'REPLY',
    }
    const {err, data}: any  = await apiClient.reaction.addReaction(queryBody);
    console.log('handleGetFollowingList info', data, err)
  }

  const handleDoLike = async () => {
    const queryBody = {
      user_id: '44895703185260544',
      target_user_id: '48811062648930304',
      target_activity_id: 'UE9TVDpQT1NUXzQ4ODExMDYyNjQ4OTMwMzA0OjE2ODQxNDA4MTYyMTMwMDA=',
      kind: 'LIKE',
    }
    const {err, data}: any  = await apiClient.reaction.addReaction(queryBody);
    console.log('handleGetFollowingList info', data, err)
  }
 
  if (!address) {
    return null
  }

  return (
    <>  
      <h2>Reaction 模块</h2>
      <p>
        <h3>获取评论列表</h3>
        <button onClick={handleGetCommentsList}>getCommentsList</button>
        <p>
          {commentsList.map((item, index) => {
            return <div key={`${item.activity_id}:${index}`}>{item.activity_id}</div>
          })}
        </p>
        <h3>贴文收藏</h3>
        <p>说明：链下收藏已成功，由于同一个帖子不能重复收藏，目前按钮点击会报异常</p>
        <button onClick={handleDoFavorite}>收藏帖子</button>
        <h3>评论: 进行中。。。</h3>
        <button onClick={handleDoComment}>评论</button>
        <h3>回复: 进行中。。。</h3>
        <button onClick={handleDoReply}>回复</button>
        <h3>点赞: 进行中。。。</h3>
        <button onClick={handleDoLike}>点赞</button>
        <p>注意：{`/v2/reaction/{id}： `}此接口可以查询评论详情暂未找到使用场景，暂未联调； 剩下两个 Delete 接口后端暂未开发完成</p>
      </p>
    </>
  )
}