import React from 'react';
import { useState } from 'react'

export const Relation = ({address, apiClient}:{address?: string, apiClient?: any})=>{

  const [followerList, setFollowerList] = useState([])
  const [followingList, setFollowingList] = useState([])

  const handleGetFollowingList = async () => {
    const feedSlug = 'feed_slug';
    const userId = '45468029551411200';
    const query = {
      address: '0x90f79bf6eb2c4f870365e785982e1f101e93b906',
      limit: 10,
    };
    const {err, data}: any  = await apiClient.feed.getFollowing(feedSlug, userId, query);
    setFollowingList(data?.obj?.rows || [])
    console.log('handleGetFollowingList info', data, err)
  }

  const handleGetFollowerList = async () => {
    const feedSlug = 'feed_slug';
    const userId = '45468029551411200';
    const query = {
      limit: 10,
    };
    const {err, data}: any  = await apiClient.feed.listFollowers(feedSlug, userId, query);
    setFollowerList(data?.obj?.rows || [])
    console.log('handleGetFollowerList info', data, err)
  }

  const handleDoFollow = async () => {
    const feedSlug = 'feed_slug';
    const userId = '44895703185260544';
    const queryBody = {
      id: '45468029551411200', // targetUserId
    };
    const params = {
      headers: {
        'On-Chain': false,
      }
    }
    const {err, data}: any  = await apiClient.feed.addRelation(feedSlug, userId, queryBody, params);
    if (data?.code === 200) {
      alert('关注成功')
    }
    console.log('handleDoFollow info', data, err)
  }

  if (!address) {
    return null
  }

  return (
    <>  
      <h2>Relation 模块</h2>
      <p>
        <h3>获取关注列表</h3>
        <button onClick={handleGetFollowingList}>getFollowingList</button>
        <p>
          {followingList.map((item, index) => {
            return <div key={`${item.owner}:${index}`}>{item.owner}</div>
          })}
        </p>
      </p>
      <p>
        <h3>获取粉丝列表</h3>
        <button onClick={handleGetFollowerList}>getFollowerList</button>
        <p>
          {followerList.map((item, index) => {
            return <div key={`${item.owner}:${index}`}>{item.owner}</div>
          })}
        </p>
      </p>
      <p>
        <h3>关注单个用户</h3>
        <button onClick={handleDoFollow}>doFollow</button>
        <p>注意：On-Chain: true 情况后端暂未开发完成</p>
      </p>
      <p>
        <h3>批量关注、取关</h3>
        <p>后端暂未开发完成</p>
        {/* addRelations 后端接口暂未实现 */}
      </p>
    </>
  )
}