import { read } from 'fs'
import { ReferenceModuleEnum, ReactionKindEnum, CollectModuleEnum } from 'osp-client-js'
import React from 'react'
import { useState } from 'react'

const metadata: any = {
  "version":"2.0.0",
  "metadata_id":"c58be35b-b036-41ff-810d-959d08685ea8",
  "content":"hihi",
  "external_url":"https://lenster.xyz/u/diwucun.test"
  ,"image": null,
  "imageMimeType":null,
  "name":"replyByWhz",
  "tags":[],
  "animation_url":null,
  "mainContentFocus":
  "TEXT_ONLY",
  "contentWarning":null,
  "attributes":[{"traitType":"type","displayType":"string","value":"text_only"}],
  "media":[],
  "locale":"zh-CN",
  "appId":"Lenster"
}

const mockCollectModule = {
  // 在帖子详情中获取
  init: {},
  type: CollectModuleEnum.FreeCollectModule
}

const mockReferenceModule = {
  // 在帖子详情中获取
  type: ReferenceModuleEnum.NULL,
  init: {}
}

const mockCollectModuleParam = {
  // 在帖子详情中获取
  init: {},
  type: CollectModuleEnum.FreeCollectModule
}

export function Reaction({client}){
  const [commentsList, setCommentsList] = useState([])
  const [replyList, setReplyList] = useState([])

  const handleGetCommentsList = async (kind) => {
    const lookupAttr = kind === ReactionKindEnum.COMMENT ? 'ACTIVITY' :'REACTION';
    const lookupValue = 
      kind === ReactionKindEnum.COMMENT
        ? 'UE9TVDpQT1NUXzUxNjQwMTA4MTA1NjI5Njk2OjE2ODQ4OTkxNDI3OTUwMDA='
        : 'Q09NTUVOVDpDT01NRU5UXzUxNjQwMTA4MTA1NjI5Njk2OjE2ODQ5MTI3MDI3NjIwMDA=';
    const query = {
      kind,
      ranking: 'TIME',
      limit: 10,
      // next_token
      with_activity: false
    };
    const {err, data}: any  = await client.reaction.get(lookupAttr, lookupValue, kind, query);
    if (kind === ReactionKindEnum.COMMENT) {
      setCommentsList(data?.obj?.rows || [])
    } else {
      setReplyList(data?.obj?.rows || [])
    }
    console.log('handleGetFollowingList info', data, err)
  }

  const handleDoFavorite = async (kind) => {
    const body = {
      target_user_id: '44897661585489920',
      target_activity_id: 'UVVFU1RJT046UVVFU1RJT05fMjAyMzA0MDcwNTUwMDIzNTUwMzUyOToxNjgxNzE5MDU5MDgzMDAw',
    }
    const options = {
      userId: '44895703185260544',
      collect_module: mockCollectModule,
      reference_module: mockReferenceModule,
      isOnChain: false
      // collect_module_param:
      // reference_module_param:
    }


    const {err, data}: any  = await client.reaction.add(kind, body, options);
    console.log('handleGetFollowingList info', data, err)
  }
 

  const handleDoFavoriteBroadcast = async () => {
    const kind = 'FAVORITE'
    const body = {
      target_user_id: '0x156',
      target_activity_id: 'UE9TVDpQT1NUXzUxNjQwMTA4MTA1NjI5Njk2OjE2ODQ4MzQ0OTc0NjkwMDA=',
    }
    const options = {
      user_id: '44895703185260544',
      collect_module: mockCollectModule,
      reference_module: mockReferenceModule,
      collect_module_param: mockCollectModuleParam,
      // reference_module_param
    }
    await client.reaction.addBroadcast(kind, body, options);
  }
  const handleDoFavoriteAbi = async () => {
    const kind = 'FAVORITE'
    const body = {
      target_user_id: '0x156',
      target_activity_id: 'UE9TVDpQT1NUXzUxNjQwMTA4MTA1NjI5Njk2OjE2ODQ4MzQ0OTc0NjkwMDA=',
    }
    const options = {
      user_id: '44895703185260544',
      collect_module: mockCollectModule,
      reference_module: mockReferenceModule,
      collect_module_param: mockCollectModuleParam,
      // reference_module_param
    }
    await client.reaction.addPostWithSig(kind, body, options);
  }
  const handleDoFavoriteCollect = async () => {
    const kind = 'FAVORITE'
    const body = {
      target_user_id: '0x156',
      target_activity_id: 'UE9TVDpQT1NUXzUxNjQwMTA4MTA1NjI5Njk2OjE2ODQ4MzQ0OTc0NjkwMDA=',
    }
    const options = {
      user_id: '44895703185260544',
      collect_module: mockCollectModule,
      reference_module: mockReferenceModule,
      collect_module_param: mockCollectModuleParam,
      // reference_module_param
    }
    await client.reaction.addPost(kind, body, options);
  }
  const handleComment = async (kind) => {
      //upload to ipfs
    const content_uri = await client.ipfs.upload(metadata)
    const body = {
      target_user_id: '51640108105629696',
      target_activity_id: 
      kind === ReactionKindEnum.COMMENT 
        ? 'UE9TVDpQT1NUXzUxNjQwMTA4MTA1NjI5Njk2OjE2ODQ4OTkxNDI3OTUwMDA='
        : 'Q09NTUVOVDpDT01NRU5UXzUxNjQwMTA4MTA1NjI5Njk2OjE2ODQ5MTI3MDI3NjIwMDA=',
      content_uri: content_uri || 'ipfs://QmYqrhL8Nzec4swV6bsHsv1ioojJUgRerszRsfeGz8VhhN'
    }
    const options = {
      userId: client.user.userId,
      isOnChain: false
    }
    const {err, data}: any  = await client.reaction.add(kind, body, options);
    console.log('HANDLECOMMENT info', data, err)
  }
  const handleGetReactionDetail = async () => {
    const activityId = document.getElementById('activityId').value
    await client.reaction.getDetail(activityId);
  }

  const handleGetCommentDetail = async () => {
    const activityId = document.getElementById('activityId').value
    await client.reaction.get('ACTIVITY',activityId, 'COMMENT' );
  }

  const getCommentParams = async (kind) => {
    const content_uri = await client.ipfs.upload(metadata)
    const profileId = client.user.profileId;
    const body = {
      target_user_id: '0x156',
      target_activity_id: 
        kind === ReactionKindEnum.COMMENT 
          ? 'UE9TVDpQT1NUXzUxNjQwMTA4MTA1NjI5Njk2OjE2ODQ4OTkxNDI3OTUwMDA='
          : 'Q09NTUVOVDpDT01NRU5UXzUxNjQwMTA4MTA1NjI5Njk2OjE2ODQ5MTI3MDI3NjIwMDA=',
      content_uri: content_uri,
      // reference_module_param:  {type: "NullReferenceModule", init: null}
    }
    const options = {
      user_id: profileId,
      isOnChain: true,
      collect_module: {
        "type": CollectModuleEnum.FreeCollectModule,
        "init": {
          "FreeCollectModule": {
            "only_follower": false
          }
        }
      }
    }
    return {
      body,
      options
    }
  }

  const handleCommentAbi = async (kind) => {
    const { body, options }: any = await getCommentParams(kind);
    client.reaction.createCommentAbi(kind, body, options);
  }
  
  const handleCommentBroadcast = async (kind) => {
    const { body, options }: any = await getCommentParams(kind);
    client.reaction.addBroadcast(kind, body, options);
  }
  return (
    <>  
      <h2>Reaction 模块</h2>
      <p>
        <h3>获取评论列表</h3>
        <button onClick={() => {handleGetCommentsList(ReactionKindEnum.COMMENT)}}>getCommentsList</button>
        <div>
          {commentsList.map((item, index) => {
            const { activity_id, id } = item
            return <div key={`${activity_id}:${index}`}>{`${id}---${index}`}</div>
          })}
        </div>
        <h3>贴文收藏: 进行中。。。</h3>
        <p>注意：切换接口的userId, activityId 可以完成收藏</p>
        <button onClick={() => {handleDoFavorite(ReactionKindEnum.FAVORITE)}}>收藏帖子</button> 

        <h3>贴文收藏: Broadcast</h3>
        <p>注意：需要等贴文详情接口返回正确的collect_module、reference_module。。。才能继续</p>
        <button onClick={handleDoFavoriteBroadcast}>收藏帖子Broadcast</button> 

        <h3>贴文收藏: 通过collectWithSig</h3>
        <button onClick={handleDoFavoriteAbi}>收藏帖子abi</button> 

        <h3>贴文收藏: 通过collect</h3>
        <button onClick={handleDoFavoriteCollect}>收藏帖子collect</button>

        <h3>发表评论</h3>
        <button onClick={() => handleComment(ReactionKindEnum.COMMENT)}>发表评论</button>

        <h3>发表评论(commentWithSig):</h3>
        <button onClick={() => { handleCommentAbi(ReactionKindEnum.COMMENT) }}>发表评论(commentWithSig)</button> 

        <h3>发表评论(Broadcast):</h3>
        <button onClick={() => { handleCommentBroadcast(ReactionKindEnum.COMMENT) }}>发表评论(Broadcast)</button> 

        <h3>获取回复列表</h3>
        <button onClick={() => {handleGetCommentsList(ReactionKindEnum.REPLY)}}>getReplyList</button>
        <div>
          {replyList.map((item, index) => {
            const { activity_id, id } = item
            return <div key={`${activity_id}:${index}`}>{`${id}---${index}`}</div>
          })}
        </div>

        <h3>发表回复</h3>
        <button onClick={() => handleComment(ReactionKindEnum.REPLY)}>发表回复</button>

        <h3>发表回复(commentWithSig):</h3>
        <button onClick={() => { handleCommentAbi(ReactionKindEnum.REPLY) }}>发表回复(commentWithSig)</button> 

        <h3>发表回复(Broadcast):</h3>
        <button onClick={() => { handleCommentBroadcast(ReactionKindEnum.REPLY) }}>发表回复(Broadcast)</button> 

        <h3>点赞</h3>
        <button onClick={() => {handleDoFavorite(ReactionKindEnum.LIKE)}}>点赞</button>
        
        <h3>Reactions信息查询</h3>
        <input type='text' id='activityId'></input>
        <button onClick={handleGetReactionDetail}>Reactions信息查询</button>

        <h3>评论信息查询</h3>
        <input type='text' id='activityId'></input>
        <button onClick={handleGetCommentDetail}>评论信息查询</button> 

      </p>
    </>
  )
}