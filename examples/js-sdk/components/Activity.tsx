import React, { useState } from 'react'
import { Client, CollectModuleEnum, ReferenceModuleEnum } from 'osp-client-js'

const metadata: any = {
    "version":"2.0.0",
    "metadata_id":"c58be35b-b036-41ff-810d-959d08685ea8",
    "content":"hihi",
    "external_url":"https://lenster.xyz/u/diwucun.test"
    ,"image": null,
    "imageMimeType":null,
    "name":"testpostwhz",
    "tags":[],
    "animation_url":null,
    "mainContentFocus":
    "TEXT_ONLY",
    "contentWarning":null,
    "attributes":[{"traitType":"type","displayType":"string","value":"text_only"}],
    "media":[],
    "locale":"zh-CN",
    "appId":"Lenster"
}

const collect = {
    "type": CollectModuleEnum.FreeCollectModule,
    "init": {
      "FreeCollectModule": {
        "only_follower": false
      }
    }
}

const reference =  {
    "type": ReferenceModuleEnum.FollowerOnlyReferenceModule,
    "init": {}
}

export const Activity = ({client}:{ client: Client })=>{
    const profileId = client.user.profileId;
    const [activityList, setActivityList] = useState([])
    const createActivity = async()=>{
			await client.activity.addActivity({
				profileId,
				metadata,
				collect,
				reference
			})
    }

    const createActivityBroadCast = async()=>{
        await client.activity.addActivityBroadCast({
            profileId,
            metadata,
            collect,
            reference
         })
    }

    // 导入测试用户 0x47e179ec197488593b187f80a00eb0da91f1b9d0b13f8733639f19c30a34926a
    const createActivityPostWithSig = async()=>{
       const res =  await client.activity.createActivityPostWithSig({
            profileId,
            metadata,
            collect,
            reference
        })
        console.log(res,'resres')
    }
    const createActivityPost = async()=>{
        await client.activity.createActivityPost({
            profileId,
            metadata,
            collect,
            reference
        })
    }
    const getAllActivities = async()=>{
        const res = await client.activity.get()
        console.log("ppppp", res)
        setActivityList(res?.data?.obj?.rows || [])
    }

    const getActivity = async(view_id)=>{
        const res = await client.activity.getActivityDetail(view_id)
        console.log("ppppp", res)
    }

    const deleteActivitiy = async()=>{
        const res = await client.activity.removeActivity('UE9TVDpQT1NUXzQ4Nzg4ODgwMjk2MjgwMDY0OjE2ODQzMzA3ODYwMjIwMDA=')
        console.log("ppppp", res)
    }

    if(!profileId) return
    return (
         <div>
            <h2>Activity模块</h2>
            <h3>创建用户的Activity</h3>
            <button onClick={createActivity}>创建Acvitity</button>
            <br />
            <h3>创建用户的Activity---BroadCast</h3>
            <button onClick={createActivityBroadCast}>创建Acvitity-BroadCast</button>
            <br />
            <h3>创建用户的Activity---通过PostWithSig创建用户</h3>
            <button onClick={createActivityPostWithSig}>创建Acvitity-abi</button>
            <br />
            <h3>创建用户的Activity---通过Post创建用户</h3>
            <button onClick={createActivityPost}>创建Acvitity-abi</button>
            <br />

            <h3>获取用户所有的Activity</h3>
            <button onClick={getAllActivities}>获取用户的所有Activity</button>
            <p>
            {activityList.map((item, index) => {
                const { view_id, title } = item;
                return <div key={`${view_id}:${index}`}>{index} --- {title} --- {view_id} <button onClick={() => {
                    getActivity(view_id);
                }}>详情</button></div>
            })}
            </p>
            <h3>删除用户的Activity —— 后端待开发</h3>
            <button onClick={deleteActivitiy}>删除用户的Activity</button>
        </div>
    )
}