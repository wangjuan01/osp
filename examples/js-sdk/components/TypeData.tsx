import { Api,CollectModuleEnum,ReferenceModuleEnum,FollowModuleEnum,ReactionKindEnum } from 'osp-client-js'



export const TypeData = ({address, apiClient}:{address?: string, apiClient?: Api<{}>})=>{
    

    const createActivity = async()=>{
        await apiClient.typedata.activityCreate({
            // profile_id/userid
            profile_id: '0xfa',
            content_uri:'ipfs://QmYqrhL8Nzec4swV6bsHsv1ioojJUgRerszRsfeGz8VhhN',
            "collect_module":
            {
                "type": CollectModuleEnum.FreeCollectModule,
                "init":
                {"FreeCollectModule":{"only_follower":false}}
            },
            "reference_module":{"type": ReferenceModuleEnum.FollowerOnlyReferenceModule,"init":{}}
        })
    }

    const relationCreate = async()=>{
        const res = await apiClient.typedata.relationCreate({
            id: '0xfa',
            "type": "user",
            "follow_module_param": {
              "type": FollowModuleEnum.FeeFollowModule,
            //   "data": {}
            }
        })
        console.log("ppppp", res)
    }

    const reactionCreate = async()=>{
        const res = await apiClient.typedata.reactionCreate({
            // profile_id/userid
            kind: ReactionKindEnum.LIKE,
            user_id: '0xfa',
            content_uri: 'ipfs://QmYqrhL8Nzec4swV6bsHsv1ioojJUgRerszRsfeGz8VhhN',
            "collect_module": {
                "type": CollectModuleEnum.FreeCollectModule,
                "init": { "FreeCollectModule": { "only_follower": false } }
            },
            "reference_module": { "type": ReferenceModuleEnum.NULL, "init": {} },
            collect_module_param:  { "type": CollectModuleEnum.FeeCollectModule, },
            reference_module_param:  { "type": ReferenceModuleEnum.NULL, },
            target_activity_id: null,
            // target_user_id:
        })
        console.log("ppppp", res)
    }

    const profileDispatcherCreate = async()=>{
        const res = await apiClient.typedata.profileDispatcherCreate({
            dispatcher: '', 
            profile_id: ''
        })
        console.log("ppppp", res)
    } 
    if(!address) return null
    return (
         <div>
            <h2>TypeData模块</h2>
            <h3>创建用户的Activity</h3>
            <button onClick={createActivity}>创建用户的Activity</button>
            <br />
            <h3>创建用户的Relation</h3>
            <button onClick={relationCreate}>创建用户的Relation</button>
            <br />
            <h3>创建用户的Reaction</h3>
            <button onClick={reactionCreate}>创建用户的Reaction</button>
            <br />
            <h3>创建用户的dispatcher</h3>
            <button onClick={profileDispatcherCreate}>创建用户的dispatcher</button>
        </div>
    )
}