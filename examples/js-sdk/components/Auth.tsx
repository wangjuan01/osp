import React from 'react'
import { Client } from 'osp-client-js'

export function Auth({client}:{ client: Client }){

  const handleSignIn = async()=>{
    const res = await client.auth.signIn()
    console.log('res', res)
  }

  return (
    <>  
        <h2>Auth模块</h2>
        <h3>登录</h3>

        <button onClick={handleSignIn}>signIn</button>
    </>
  )
}