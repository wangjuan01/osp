import React from 'react'
import { useState } from 'react'
import { Client,FollowModuleEnum,User as UserType } from 'osp-client-js'

export const User = ({client}:{ client: Client })=>{
    const [form, setForm] = useState(({
        follow_module: FollowModuleEnum.NULL,
        follow_nft_recived:'',
        follow_nft_currency: '',
        follow_nft_amount:''
    }))

    const [profileList, setProfileList] = useState<UserType[]>([])

    const createProfiles = async()=>{
        console.log(form,'formform')
        const { follow_module,follow_nft_currency,follow_nft_amount,follow_nft_recived, ...rest } = form
        const res = await client.user.create({
            follow_module:{
                type: follow_module,
                init: follow_module === FollowModuleEnum.NULL ? {} : {
                    FeeFollowModule:{
                        amount:{
                            currency: follow_nft_currency,
                            value: follow_nft_amount
                        },
                        recipient: follow_nft_recived,
                    }
                },
            },
            ...rest
        })
        console.log('res', res)
    }
    const getAllProfiles = async()=>{
        const list = await client.user.getAllProfile()
        if(!list) return
        setProfileList(list)
    }

    const getProfile = async(profileId)=>{
        const data = await client.user.get(profileId)
        alert(JSON.stringify(data))
    } 

    const update = async()=>{
        const userID = '0xf7'
        const res = await client.user.update(userID, 'bio', 'ggg')
        console.log("ppppp", res)
    } 

    const formChange = (key,value)=> setForm({...form, [key]: value})

    const typeChange = (e)=>{
        if(!(e.target.value === FollowModuleEnum.FeeFollowModule)){
            formChange('follow_nft_currency',undefined)
            formChange('follow_nft_amount',undefined)
        }
        formChange('follow_module', e.target.value);
    }

    return (
         <div>
            <h2>User模块</h2>
            <h3>创建用户的profile</h3>
            类型: 
            <select onChange={typeChange} value={form.follow_module}>
                <option value="FeeFollowModule">付费关注</option>
                <option value="ProfileFollowModule">已有profile</option>
                <option value="RevertFollowModule">拒接关注</option>
                <option value="NullFollowModule">空</option>
            </select>
            {
                form.follow_module === FollowModuleEnum.FeeFollowModule && (
                    <>
                    <br />
                    币种: <input onChange={(e)=> formChange('follow_nft_currency',e.target.value)}/>
                    <br />
                    数量: <input onChange={(e)=> formChange('follow_nft_amount',e.target.value)}/>
                    <br />
                    接受地址: <input onChange={(e)=> formChange('follow_nft_recived',e.target.value)}/>
                    </>
                )
            }
            <br />
            用户名: <input onChange={(e)=> formChange('handle',e.target.value)}/>
            <br />
            简介: <input onChange={(e)=> formChange('bio',e.target.value)}/>
            <br />
            {/* 头像为必传 */}
            头像: <input onChange={(e)=> formChange('avatar',e.target.value)}/>
            <br />
            背景: <input onChange={(e)=> formChange('cover_picture',e.target.value)}/>
            <br />
            <p>注意：头像为必传， 用户名不能少于5位</p>
            <button onClick={createProfiles}>创建用户的profile</button>
            <br />
            <h3>获取用户所有的profile</h3>
            <button onClick={getAllProfiles}>获取用户的所有profile</button>
            {
                profileList.map(i=> (
                    <>
                        <br />
                        <span>{i.profile_id}</span>
                        <button onClick={()=> getProfile(i.profile_id)}>获取用户的profile详情</button>
                    </>
                ))
            }
        
            {/* 
            todo:
            <h3>更新用户信息</h3>
            <button onClick={update}>更新用户</button> */}
            

        </div>
    )
}