module.exports = {
  reactStrictMode: true,
  transpilePackages: ["ui"],
  async rewrites() {
    return [
      {
        source: '/api/:path*',
        destination: 'http://dev.opensocial.trex.xyz/:path*',
      },
    ]
  },


};
