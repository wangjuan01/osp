
// import {  Auth, User, Activity, Relation, Reaction } from '../../components/index'

import dynamic from 'next/dynamic'
import React from 'react'

const ConnectWallet = dynamic(
  // @ts-ignore 
  () => import('../../components/index').then((T) => T.ConnectWallet),
  {
    ssr: false
  }
) 
const Auth = dynamic(
  () => import('../../components/index').then((T) => T.Auth),
  {
    ssr: false
  }
) 
const User = dynamic(
  () => import('../../components/index').then((T) => T.User),
  {
    ssr: false
  }
) 
const Activity = dynamic(
  () => import('../../components/index').then((T) => T.Activity),
  {
    ssr: false
  }
) 
const Relation = dynamic(
  () => import('../../components/index').then((T) => T.Relation),
  {
    ssr: false
  }
) 
const Reaction = dynamic(
  () => import('../../components/index').then((T) => T.Reaction),
  {
    ssr: false
  }
) 

export function App(props) {
  return (
  	<div>
  	  <h1>JS SDK Example：</h1>
      <Auth {...props}/>
      <User {...props}/>
      <Activity {...props}/>
      <Relation {...props}/>
      <Reaction {...props}/>
      
    </div>
  )
}