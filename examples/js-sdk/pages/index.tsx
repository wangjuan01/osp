import React from 'react';
import { App } from '../widget/app/App'
import { connect } from 'osp-client-js'
import hub from '../abi/LensHub.json'
import { AbiItem } from 'web3-utils'

const options = {
  urlOverride: {
    api: typeof location !== 'undefined' ? location?.origin  + '/api/v2': ''
  },
  timeout: 10000,
  browser: true,
  version: ''
};

export default function Example() {
  const abi = hub.abi as AbiItem[]
  const client = connect(
      'key',
      '',
      'app_id',
      options,
      {
        abi
      }
  )
  console.log('client', client.address,'sadasdasd')
  return (
    <App client={client}/>
  )
}

/**
 * TODO LIST（后端暂未完成）:
 * 
 *  Auth模块
 *    暂无
 * 
 *  User模块
 *    1. 删除Profile
 *    2. 更新Profile
 *    3. user dispatcher
 * 
 *  Activity模块
 *    1. 删除用户的Activity
 * 
 *  Relation模块
 *    1. 取消关注
 *    2. 批量关注
 * 
 *  Reaction模块
 *    1. 删除评论 & 回复
 *    2. 取消收藏
 *    3. 点赞（kind = LIKE），不上链的已调完，上链的后端暂未开发
 *    4. 取消点赞
 *    4. 分享（kind = SHARE）
 *    5. 投票（kind = UPVOTE）
 *    6. 取消投票（kind = DONWVOTE）
 * 
 *  TypeData模块
 *    1. /v2/typedata/profile/dispatcher
 */