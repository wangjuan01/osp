import { CollectType, ReferenceType } from "./activity";
import { DefaultGenerics,Client } from "./client";
import { ReactionKindEnum,
  AddReactionRequest,
  CollectModule,
  CollectModuleEnum,
  ReferenceModuleParam,
  ReferenceModuleEnum,
  ReferenceModule,
  CollectModuleParam,
  ListReactionRequest
 } from "./rest_api_generated";
 import { ethers } from 'ethers'

export type ReactionAddActivity = {
  target_user_id: string,
  target_activity_id: string,
  content_uri: string,
  reference_module_param: ReferenceModuleParam,
}
export type ReactionAddOptions = {
  userId: string,
  collect: CollectModule,
  reference: ReferenceModule,
  collectParam: any,
  referenceParam: any,
  isOnChain: boolean
}

export type ReactionCreateActivity = {
  target_user_id: string,
  target_activity_id: string,
  content_uri: string,
  reference_module_param: ReferenceModuleParam,
}
export type ReactionCreateOptions = {
  user_id: string,
  collect_module?: CollectModule;
  reference_module?: ReferenceModule;
  collect_module_param: CollectModuleParam;
  reference_module_param: ReferenceModuleParam;

}
const mockCollectModule = {
  "type": CollectModuleEnum.FeeCollectModule,
  "init": {
    "FreeCollectModule": {
      "only_follower": true
    }
  }
}
const mocReferenceModule = {
  "type": ReferenceModuleEnum.FollowerOnlyReferenceModule,
  "init": {}
}

const mockCollectModuleParam = {
  "type": CollectModuleEnum.FeeCollectModule,
  "data": {}
}

const mocReferenceModuleParam = {
  "type": ReferenceModuleEnum.FollowerOnlyReferenceModule,
  "data": {}
}

export class Reaction<StreamFeedGenerics extends DefaultGenerics = DefaultGenerics> {
  client: Client<StreamFeedGenerics>;
  constructor(client: Client<StreamFeedGenerics>) {
    this.client = client;
  }

  /**
   * add reaction
   * @link https://getstream.io/activity-feeds/docs/node/reactions_introduction/?language=js#adding-reactions
   * @method addBroadcast
   * @memberof StreamReaction.prototype
   * @param  {string}   kind  kind of reaction
   * @param  {string}   activity Activity or an ActivityID
   * @param  {ReactionType}   data  data related to reaction
   * @param  {ReactionAddOptions} [options]
   * @param  {string} [options.id] id associated with reaction
   * @param  {string[]} [options.targetFeeds] an array of feeds to which to send an activity with the reaction
   * @param  {string} [options.userId] useful for adding reaction with server token
   * @param  {object} [options.targetFeedsExtraData] extra data related to target feeds
   * @return {Promise<ReactionAPIResponse<ReactionType>>}
   * @example reactions.add("like", "0c7db91c-67f9-11e8-bcd9-fe00a9219401")
   * @example reactions.add("comment", "0c7db91c-67f9-11e8-bcd9-fe00a9219401", {"text": "love it!"},)
   */
  async addBroadcast (
    kind: ReactionKindEnum,
    activity: ReactionCreateActivity,
    data: ReactionCreateOptions
  ) {
  const body: AddReactionRequest = {
    kind,
    target_activity_id: activity?.target_activity_id,
    target_user_id: activity?.target_user_id,
    content_uri: activity?.content_uri,
    user_id: data.user_id,
    collect_module: data.collect_module || mockCollectModule,
    reference_module: data.reference_module || mocReferenceModule,
    collect_module_param: data.collect_module_param || mockCollectModuleParam,
    reference_module_param: data.reference_module_param || mocReferenceModuleParam
  };

  const res = await this.client.typedata.reactionCreate(body);
  console.log('line101', res);
  const type_data = res.data.obj.type_data
  const broadcastId = res.data.obj.broadcast_id
  let signature = ''
  var params = [this.client.address, JSON.stringify(type_data)];
  var method = 'eth_signTypedData_v4';
  // const signature = await this.client.web3.eth.personal.sign(signMessage, );
  if(typeof this.client.web3.currentProvider === "string") return
  const provider = this.client.web3.currentProvider
  if(!('sendAsync' in provider)) return
  provider.sendAsync({
    method,
    params,
    // @ts-ignore
    from: this.client.address
  },(err, data)=>{
    signature  = data.result
    if(signature){
      this.client.broadcast.create(broadcastId , signature).then(res_broadcase=>{
        console.log('res_broadcase', res_broadcase)
      });
    }
  })
    return res
  }

  async addPostWithSig (
    kind: ReactionKindEnum,
    activity: ReactionCreateActivity,
    data: ReactionCreateOptions
  ) {
  const body: AddReactionRequest = {
    kind,
    target_activity_id: activity?.target_activity_id,
    target_user_id: activity?.target_user_id,
    content_uri: activity?.content_uri,
    user_id: data.user_id,
    collect_module: data.collect_module || mockCollectModule,
    reference_module: data.reference_module || mocReferenceModule,
    collect_module_param: data.collect_module_param || mockCollectModuleParam,
    reference_module_param: data.reference_module_param || mocReferenceModuleParam
  };

  const res = await this.client.typedata.reactionCreate(body);
  console.log('line101', res);
  const { obj:{ type_data, type_data:{ message } } } = res.data
  let signature = ''
  var params = [this.client.address, JSON.stringify(type_data)];
  var method = 'eth_signTypedData_v4';
  // const signature = await this.client.web3.eth.personal.sign(signMessage, );
  if(typeof this.client.web3.currentProvider === "string") return
  const provider = this.client.web3.currentProvider
  if(!('sendAsync' in provider)) return
  provider.sendAsync({
    method,
    params,
    // @ts-ignore
    from: this.client.address
  },(err, data)=>{
    signature  = data.result
    const { r, s, v } = ethers.utils.splitSignature(signature);
    const { pubI } = message;
    this.client.contract.methods.collectWithSig({
      collector: this.client.address,
      profileId: message.profileId,
      pubId: message.pubId,
      data: message.data,
      sig:{
        v,
        r,
        s,
        deadline:message.deadline
      }
    }).send().then(() => {
      console.log('Transaction complete!');
    }).catch((error: string) => {
      console.error('Error occurred:', error);
    })
    console.log(this.client.contract,'dddddddd',data)
  })
    return res
  }
  async addPost (
    kind: ReactionKindEnum,
    activity: ReactionCreateActivity,
    data: ReactionCreateOptions
  ) {
  const body: AddReactionRequest = {
    kind,
    target_activity_id: activity?.target_activity_id,
    target_user_id: activity?.target_user_id,
    content_uri: activity?.content_uri,
    user_id: data.user_id,
    collect_module: data.collect_module || mockCollectModule,
    reference_module: data.reference_module || mocReferenceModule,
    collect_module_param: data.collect_module_param || mockCollectModuleParam,
    reference_module_param: data.reference_module_param || mocReferenceModuleParam
  };

  const res = await this.client.typedata.reactionCreate(body);
  console.log('line101', res);
  const { obj:{ type_data:{ message } } } = res.data
  this.client.contract.methods.collect(
    message.profileId,
    message.pubId,
    message.data
  ).send().then(() => {
    console.log('Transaction complete!');
  }).catch((error: string) => {
    console.error('Error occurred:', error);
  })
    return res
  }
    /**
   * add reaction
   * @link https://getstream.io/activity-feeds/docs/node/reactions_introduction/?language=js#adding-reactions
   * @method add
   * @memberof StreamReaction.prototype
   * @param  {string}   kind  kind of reaction
   * @param  {string}   activity Activity or an ActivityID
   * @param  {ReactionType}   data  data related to reaction
   * @param  {ReactionAddOptions} [options]
   * @param  {string} [options.id] id associated with reaction
   * @param  {string[]} [options.targetFeeds] an array of feeds to which to send an activity with the reaction
   * @param  {string} [options.userId] useful for adding reaction with server token
   * @param  {object} [options.targetFeedsExtraData] extra data related to target feeds
   * @return {Promise<ReactionAPIResponse<ReactionType>>}
   * @example reactions.add("like", "0c7db91c-67f9-11e8-bcd9-fe00a9219401")
   * @example reactions.add("comment", "0c7db91c-67f9-11e8-bcd9-fe00a9219401", {"text": "love it!"},)
   */
     async add (
      kind: ReactionKindEnum,
      {
        target_user_id,
        target_activity_id,
        content_uri,
        reference_module_param
      }: ReactionAddActivity,
      {
        userId,
        collect,
        reference,
        collectParam,
        referenceParam,
        isOnChain
      }: ReactionAddOptions
    ) {
  
      const body:AddReactionRequest = {
        kind,
        target_activity_id,
        target_user_id,
        content_uri,
        user_id: userId,
        collect_module: collect || mockCollectModule,
        // reference_module: reference || {
        //   "type": ReferenceModuleEnum.FollowerOnlyReferenceModule,
        //   "init": {}
        // },
        // collect_module: reference || {
        //   "type": ReferenceModuleEnum,
        //   "init": {}
        // },
        collect_module_param: collectParam || mockCollectModuleParam,
        reference_module_param: reference_module_param || mocReferenceModuleParam
      };
      const params = {
        headers: {
          'On-Chain': `${isOnChain}`,
        }
      }
      const res = await this.client.request.reaction.addReaction(body, params)
      return res
    }


    async createBroadcast (
      kind: ReactionKindEnum,
      activity: ReactionCreateActivity,
      data: ReactionCreateOptions
    ) {
  
    const body: AddReactionRequest = {
      kind,
      target_activity_id: activity?.target_activity_id,
      target_user_id: activity?.target_user_id,
      content_uri: activity?.content_uri,
      user_id: data.user_id,
      collect_module: data.collect_module || {
        "type": CollectModuleEnum.FeeCollectModule,
        "init": {
          "FreeCollectModule": {
            "only_follower": true
          }
        }
      },
      reference_module: data.reference_module || {
        "type": ReferenceModuleEnum.FollowerOnlyReferenceModule,
        "init": {}
      },
      collect_module_param: data.collect_module_param || {
        "type": CollectModuleEnum.FeeCollectModule,
        "data": {}
      },
      reference_module_param: data.reference_module_param || {
        "type": ReferenceModuleEnum.FollowerOnlyReferenceModule,
        "data": {}
      }
    };
  
    
  
  
    const res = await this.client.typedata.reactionCreate(body)
    const type_data = res.data.obj.type_data
    const broadcastId = res.data.obj.broadcast_id
  
    // const res = await this.client.request.reaction.addReaction(body, params)
    return res
  }
  /**
   * get all reaction
   * @link https://getstream.io/activity-feeds/docs/node/reactions_introduction/?language=js#retrieving-reactions
   * @method get
   * @memberof StreamReaction.prototype
   * @param  {string}   id Reaction Id
   * @return {Promise<EnrichedReactionAPIResponse<StreamFeedGenerics>>}
   * @example reactions.get("ACTIVITY","67b3e3b5-b201-4697-96ac-482eb14f88ec","COMMENT", {limit:20})
   */
  async get(lookupAttr: string, lookupValue: string, kind: ReactionKindEnum, data: { ranking: any; limit: any; withActivity: any; }) {

    const query = {
      kind: kind,
      ranking: data?.ranking || 'TIME',
      limit: data?.limit || 10,
      with_activity: data?.withActivity || false
    } as any;

    const res = await this.client.request.reaction.getReactions(lookupAttr, lookupValue, kind, query);
    return res
  }
  /**
   * get reaction detail
   * @link https://getstream.io/activity-feeds/docs/node/reactions_introduction/?language=js#retrieving-reactions
   * @method get
   * @memberof StreamReaction.prototype
   * @param  {string}   id Reaction Id
   * @return {Promise<EnrichedReactionAPIResponse<StreamFeedGenerics>>}
   * @example reactions.get("67b3e3b5-b201-4697-96ac-482eb14f88ec")
   */
  async getDetail(id: string) {
    const res = await this.client.request.reaction.getReaction(id);
  }
  /**
   * delete reaction
   * @link https://getstream.io/activity-feeds/docs/node/reactions_introduction/?language=js#removing-reactions
   * @method delete
   * @memberof StreamReaction.prototype
   * @param  {string}   id Reaction Id
   * @return {Promise<APIResponse>}
   * @example reactions.delete("ACTIVITY","67b3e3b5-b201-4697-96ac-482eb14f88ec","COMMENT")
   */
  async deleteByKind(
      lookupAttr: string,
      lookupValue: string,
      kind: ReactionKindEnum
    ) {
    const res = await this.client.request.reaction.deleteReaction(lookupAttr);
    return res
  }
  /**
   * delete reaction
   * @link https://getstream.io/activity-feeds/docs/node/reactions_introduction/?language=js#removing-reactions
   * @method delete
   * @memberof StreamReaction.prototype
   * @param  {string}   id Reaction Id
   * @return {Promise<APIResponse>}
   * @example reactions.delete("67b3e3b5-b201-4697-96ac-482eb14f88ec")
   */
  async delete(id: string) {
    const res = await this.client.request.reaction.deleteReaction(id);
  }

  async createCommentAbi (kind: ReactionKindEnum, activity: ReactionCreateActivity, queryData: ReactionCreateOptions) {
    const body: AddReactionRequest = {
      kind,
      target_activity_id: activity?.target_activity_id,
      target_user_id: activity?.target_user_id,
      content_uri: activity?.content_uri,
      user_id: queryData.user_id,
      collect_module: queryData.collect_module || {
        "type": CollectModuleEnum.FreeCollectModule,
        "init": {
          "FreeCollectModule": {
            "only_follower": true
          }
        }
      },
      reference_module: queryData.reference_module || {
        "type": ReferenceModuleEnum.FollowerOnlyReferenceModule,
        "init": {}
      },
      collect_module_param: queryData.collect_module_param || {
        "type": CollectModuleEnum.FeeCollectModule,
        "data": {}
      },
      reference_module_param: queryData.reference_module_param || {
        "type": ReferenceModuleEnum.FollowerOnlyReferenceModule,
        "data": {}
      }
    };
    const { data, error }  = await this.client.typedata.reactionCreate(body)
    if(error) return
    const { obj:{ type_data,type_data:{ message } } } = data

    const params = [this.client.address, JSON.stringify(type_data)];
    const method = 'eth_signTypedData_v4';
    let signature = ''
    if(typeof this.client.web3.currentProvider === "string") return
    const provider = this.client.web3.currentProvider
    if(!('sendAsync' in provider)) return
    provider?.sendAsync({
      method,
      params,
      // @ts-ignore
      from: this.client.address
    },(err: any, data: { result: string })=>{
      if (err) {
        console.log('error:::', err)
        return;
      }
      signature  = data.result
      // 拆分签名结果为 r, s, v 值
      const { r, s, v } = ethers.utils.splitSignature(signature);

      this.client.contract.methods.commentWithSig({
        profileId: message.profileId,
        contentURI: message.contentURI,
        profileIdPointed: message.profileIdPointed,
        pubIdPointed: message.pubIdPointed,
        referenceModuleData: message.referenceModuleData,
        collectModule: message.collectModule,
        collectModuleInitData:message.collectModuleInitData,
        referenceModule: message.referenceModule,
        referenceModuleInitData: message.referenceModuleInitData,
        sig:{
          v,
          r,
          s,
          deadline:message.deadline
        }
      }).send().then(() => {
        console.log('Transaction complete!');
      }).catch((error: string) => {
        console.error('Error occurred:', error);
      })
        console.log(this.client.contract,'dddddddd',data)
      })
}

}