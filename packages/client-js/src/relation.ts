import { ethers } from 'ethers'

import { DefaultGenerics,Client } from "./client";
import { FollowModuleParam, CollectModule, ReferenceModule } from "./rest_api_generated";

type ICreateRelationPost = {
  userId: string;
  feedSlug: string;
  data?: {
    followModuleParam: FollowModuleParam
  }
}

export class Relation<StreamFeedGenerics extends DefaultGenerics = DefaultGenerics> {
  client: Client<StreamFeedGenerics>;
  constructor(client: Client<StreamFeedGenerics>) {
    this.client = client;
  }

  /**
   * @link https://getstream.io/activity-feeds/docs/node/following/?language=js
   * @method follow
   * @memberof StreamFeed.prototype
   * @param  {string}   targetSlug   Slug of the target feed
   * @param  {string}   targetUserId User identifier of the target feed
   * @param  {object}   [options]      Additional options
   * @param  {number}   [options.limit] Limit the amount of activities copied over on follow
   * @return {Promise<APIResponse>}
   * @example client.follow('user', 'OX001');
   */
  async follow(feedSlug: string, userId: string, data: { followModuleParam: any; isOnChain: any; targetUserId: string; }) {
    const body = {
      id: data.targetUserId,
      type: feedSlug,
      follow_module_param: data?.followModuleParam || {
        "type": "FeeFollowModule",
         amount: {
            currency: "0xD40282e050723Ae26Aeb0F77022dB14470f4e011",
            value: "0.01"
         }
      }
    }
    const params = {
      headers: {
        'On-Chain': data?.isOnChain
      }
    }
    const res = await this.client.request.feed.addRelation(feedSlug, userId, body, params);
    return res
  }

  async followBroadcast(feedSlug: string, userId: string, data: {followModuleParam: FollowModuleParam}) {
    const body = {
      id: userId,
      type: feedSlug,
      follow_module_param: data?.followModuleParam
    }


    const res = await this.client.typedata.relationCreate(body)
    const type_data = res.data.obj.type_data
    const broadcastId = res.data.obj.broadcast_id
    //get follow sign

    var params = [this.client.address, JSON.stringify(type_data)];
    var method = 'eth_signTypedData_v4';
    let signature = ''
    // const signature = await this.client.web3.eth.personal.sign(signMessage, );
    if(typeof this.client.web3.currentProvider === "string") return
    const provider = this.client.web3.currentProvider
    if(!('sendAsync' in provider)) return
    provider.sendAsync({
      method,
      params,
      // @ts-ignore
      from: this.client.address
    },(err, data)=>{
      signature  = data.result
      if(signature){
        this.client.broadcast.create(broadcastId , signature).then(res_broadcase=>{
          console.log('res_broadcase', res_broadcase)
        });
      }


    })

  }

  async createRelationPostWithSig ({ userId, feedSlug, data: moduleParamData }: ICreateRelationPost){
    const body = {
      id: userId,
      type: feedSlug,
      follow_module_param: moduleParamData?.followModuleParam
    }
    const { data, error } = await  this.client.typedata.relationCreate(body)
    if(error) return
    const { obj:{ type_data,type_data:{ message } } } = data

    const params = [this.client.address, JSON.stringify(type_data)];
    const method = 'eth_signTypedData_v4';
    let signature = ''
    if(typeof this.client.web3.currentProvider === "string") return
    const provider = this.client.web3.currentProvider
    if(!('sendAsync' in provider)) return
    provider?.sendAsync({
      method,
      params,
      // @ts-ignore
      from: this.client.address
    },(err: any, data: { result: string })=>{
      signature  = data.result
      // 拆分签名结果为 r, s, v 值
      const { r, s, v } = ethers.utils.splitSignature(signature);
      const { datas, profileIds, deadline } = message;
      this.client.contract.methods.followWithSig({
        datas,
        profileIds,
        follower: this.client.address,
        sig:{
          v,
          r,
          s,
          deadline
        }
      }).send().then(() => {
        console.log('Transaction complete!');
      }).catch((error: string) => {
        console.error('Error occurred:', error);
      })
        console.log(this.client.contract,'dddddddd',data)
      })
  }

  async createRelationPost ({ userId, feedSlug, data: moduleParamData }: ICreateRelationPost){
    const body = {
      id: userId,
      type: feedSlug,
      follow_module_param: moduleParamData?.followModuleParam
    }
    const { data, error } = await this.client.typedata.relationCreate(body)
    if(error) return
    const { obj:{ type_data:{ message } } } = data;
    const { datas, profileIds } = message;
    this.client.contract.methods.follow(profileIds, datas).send().then(() => {
      console.log('Transaction complete!');
    }).catch((error: string) => {
      console.error('Error occurred:', error);
    })

  }

  /**
   * @link https://getstream.io/activity-feeds/docs/node/following/?language=js
   * @method followMany
   * @memberof StreamFeed.prototype
   * @param  {string}   targetSlug   Slug of the target feed
   * @param  {string}   targetUserId User identifier of the target feed
   * @param  {object}   [options]      Additional options
   * @param  {number}   [options.limit] Limit the amount of activities copied over on follow
   * @return {Promise<APIResponse>}
   * @example client.follow('user', 'OX001');
   */
  followMany(feedSlug: string, userId: string | { id: string }) {
    //TODO
  }

  /**
   * List the user following list
   * @link https://getstream.io/activity-feeds/docs/node/following/?language=js#reading-followed-feeds
   * @method following
   * @memberof StreamFeed.prototype
   * @param  {GetFollowOptions}   [options]  Additional options
   * @param  {string[]}   options.filter array of feed id to filter on
   * @param  {number}   options.limit pagination
   * @param  {number}   options.offset pagination
   * @return {Promise<GetFollowAPIResponse>}
   * @example feed.following('user', 'OX11', {limit:10});
   */
  async following(feedSlug: string, userId: string, data: { address: string; limit: number; nextToken: string; }) {
    const query = {
      address: data?.address,
      limit: data?.limit || 10,
      next_token: data?.nextToken
    };

    const res = await this.client.request.feed.getFollowing(feedSlug, userId, query);
    return res
  }

  /**
   * List the user followers list
   * @link https://getstream.io/activity-feeds/docs/node/following/?language=js#reading-feed-followers
   * @method followers
   * @memberof StreamFeed.prototype
   * @param  {GetFollowOptions}   [options]  Additional options
   * @param  {string[]}   options.filter array of feed id to filter on
   * @param  {number}   options.limit pagination
   * @param  {number}   options.offset pagination
   * @return {Promise<GetFollowAPIResponse>}
   * @example feed.followers({limit:10, filter: ['user:1', 'user:2']});
   */
  async followers(feedSlug: string, userId: string, data: { limit: number; nextToken: string; }) {
    const query = {
      limit: data?.limit || 10,
      next_token: data?.nextToken || ''
    };

    const res = await this.client.request.feed.listFollowers(feedSlug, userId, query);
    return res
  }


}