import { ThirdwebStorage } from "@thirdweb-dev/storage";
import { DefaultGenerics,Client } from "./client";
const storage = new ThirdwebStorage();

export class IPFS<StreamFeedGenerics extends DefaultGenerics = DefaultGenerics> {
  client: Client<StreamFeedGenerics>;
  constructor(client: Client<StreamFeedGenerics>) {
    this.client = client
  }
  async upload(metadata: unknown){
    // const metadata = { name: "Hello world" };
    const uri = await storage.upload(metadata);
    console.log(uri)
    return uri
  }


}