/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface CommonResponse {
  /** error code */
  code?: number;
  /** error msg */
  msg?: string;
  obj?: object;
}

/** device type enum */
export enum DeviceTypeEnum {
  UNKNOWN = "UNKNOWN",
  ANDROID = "ANDROID",
  IOS = "IOS",
  WEB = "WEB",
  MANAGER = "MANAGER",
}

export type TransactionResponse = CommonResponse & {
  obj?: {
    /** txHash if success */
    tx_hash?: string;
    /** txId if indexed by relay */
    tx_id?: string;
    info?: object;
  };
};

export interface Challenge {
  /** text to sign */
  text?: string;
}

export type ChallengeResponse = CommonResponse & {
  obj: Challenge;
};

export interface Token {
  /** 30 minutes access token */
  access_token?: string;
  /** 7 days refresh token */
  refresh_token?: string;
}

export type TokenResponse = CommonResponse & {
  obj?: Token;
};

export interface AuthRequest {
  /** sign message by challenge text */
  signature: string;
  /** address */
  address: string;
}

export interface Activity {
  /** user info */
  user?: User;
  /** @format uri */
  content_uri?: string;
  reference_module?: ReferenceModule;
  collect_module?: CollectModule;
  /** @format int64 */
  created?: number;
  /** @format int64 */
  share_count?: number;
  /** @format int64 */
  like_count?: number;
  /** @format int64 */
  comment_count?: number;
  /** @format int64 */
  reply_count?: number;
  /** @format int64 */
  favorite_count?: number;
  /** @format int64 */
  view_count?: number;
  /** @format int64 */
  upvote_count?: number;
  /** @format int64 */
  downvote_count?: number;
}

export type ActivityResponse = CommonResponse & {
  obj?: Activity;
};

export type ActivityPaginationResponse = CommonResponse & {
  obj?: {
    /**
     * @format int32
     * @min 1
     * @max 20
     */
    limit?: number;
    /** @format int32 */
    total?: number;
    next_token?: string;
    rows?: Activity[];
  };
};

export interface AddActivityRequest {
  /**
   * user id
   * @format string
   */
  profile_id?: string;
  /**
   * The metadata uploaded somewhere passing in the url to reach it
   * @format uri
   */
  content_uri?: string;
  collect_module?: CollectModule;
  reference_module?: ReferenceModule;
}

/** activity category */
export enum ActivityCategoryEnum {
  POST = "POST",
  QUESTION = "QUESTION",
  ANSWER = "ANSWER",
}

/** feed ranking enum */
export enum ActivityRankingEnum {
  ACTIVITY_TIME = "ACTIVITY_TIME",
  TOP_COLLECTED = "TOP_COLLECTED",
  TOP_COMMENTED = "TOP_COMMENTED",
  TOP_REPOSTED = "TOP_REPOSTED",
}

export interface GetActivitiesRequest {
  /**
   * The amount of activities requested from the APIs
   * @format int32
   */
  limit?: number;
  /** nextToken */
  next_token?: string;
  /** filter activity category */
  categories: ActivityCategoryEnum[];
  /** exclude profile ids */
  exclude_user_ids?: string[];
  /** turn on or off randomizer, not supported */
  randomize?: boolean;
  /** feed ranking enum */
  ranking?: ActivityRankingEnum;
  /** The tenant Ids */
  tenants?: string[];
  /**
   * The tenant Ids
   * @format int64
   */
  timestamp?: number;
}

/** user info */
export interface User {
  /** user id */
  profile_id?: string;
  /** display name */
  name?: string;
  handle?: string;
  bio?: string;
  /** picture(url,height/width/mimeType...) or NftImage(contractAddress,tokenId,uri) */
  avatar?: string;
  /** cover picture (original/small/medium) same with avatar */
  cover_picture?: string;
  /** is default user by owner */
  default?: boolean;
  /** todo support? */
  metadata?: object;
  /** owner address */
  owner?: string;
  attributes?: UserAttribute[];
  dispatcher?: Dispatcher;
  /** The follow Module */
  follow_module?: object;
  /** Follow nft address */
  follow_nft_address?: string;
  /** isFollowing */
  following?: boolean;
  /** isFollowed */
  followed?: boolean;
  /** @format int64 */
  following_count?: number;
  /** @format int64 */
  follower_count?: number;
  /** @format int64 */
  subscribe_count?: number;
  /** @format int64 */
  activity_count?: number;
  /** @format int64 */
  join_count?: number;
  /** @format int64 */
  activity_like_count?: number;
}

export interface UserAttribute {
  /** The display type */
  display_type?: string;
  /** identifier of this attribute, we will update by this id */
  key?: string;
  /** The trait type - can be anything its the name it will render so include spaces */
  trait_type?: string;
  /** Value attribute */
  value?: string;
}

export type UserResponse = CommonResponse & {
  /** user info */
  obj?: User;
};

export type UserPaginationResponse = CommonResponse & {
  obj?: {
    /**
     * @format int32
     * @min 1
     * @max 20
     */
    limit?: number;
    /** @format int32 */
    total?: number;
    next_token?: string;
    rows?: User[];
  };
};

/** user status */
export enum UserStatusEnum {
  CreateSetDispatcherEIP712TypedDataValue = "CreateSetDispatcherEIP712TypedDataValue",
}

export interface Dispatcher {
  /** wallet address of dispatcher */
  address?: string;
  /** use the built-in dispatcher on the API */
  canRelay?: boolean;
}

export interface GetUserRequest {
  /** user owner wallet address */
  address?: string;
  /** user handle */
  handles?: string[];
  /** user id */
  ids?: string[];
  /** filter default user */
  filter_default?: boolean;
  /** nextToken for pagination */
  next_token?: string;
  /** limit for pagination */
  limit?: string;
}

export interface Amount {
  /** currency contract address */
  currency?: string;
  /** Floating point number as string. It could have the entire precision of the Asset or be truncated to the last significant decimal */
  value?: string;
}

export interface RecipientPercent {
  /** recipient contract address */
  recipient?: string;
  /**
   * should be between 1 and 100
   * @format float
   */
  percent?: number;
}

/** collect module enum */
export enum CollectModuleEnum {
  FeeCollectModule = "FeeCollectModule",
  FreeCollectModule = "FreeCollectModule",
  LimitedFeeCollectModule = "LimitedFeeCollectModule",
  LimitedTimedFeeCollectModule = "LimitedTimedFeeCollectModule",
  RevertCollectModule = "RevertCollectModule",
  TimedFeeCollectModule = "TimedFeeCollectModule",
  NULL = "NullReferenceModule",
}

export interface CollectModule {
  /** collect module enum */
  type: CollectModuleEnum;
  /** You only have to enter one   */
  init?: {
    FreeCollectModule?: FreeCollectModule;
    FeeCollectModule?: FeeCollectModule;
    LimitedFeeCollectModule?: LimitedFeeCollectModule;
    LimitedTimedFeeCollectModule?: LimitedTimedFeeCollectModule;
    RevertCollectModule?: object;
    TimedFeeCollectModule?: TimedFeeCollectModule;
  } | null;
}

export interface FeeCollectModule {
  amount?: Amount;
  /** only for my follower */
  only_follower?: boolean;
  /** recipient address */
  recipient?: string;
  /** @format float */
  referral_fee?: number;
}

export interface FreeCollectModule {
  /** only for my follower */
  only_follower: boolean;
}

export interface LimitedFeeCollectModule {
  amount: Amount;
  collect_limit: number;
  /** only for my follower */
  only_follower: boolean;
  /** recipient address */
  recipient: RecipientPercent[];
  /** @format float */
  referral_fee: number;
}

export type LimitedTimedFeeCollectModule = LimitedFeeCollectModule & {
  /** 目前暂未使用 */
  deadline?: string;
};

export type TimedFeeCollectModule = FeeCollectModule & {
  /** UnixTimestamp for deadline 目前未使用 */
  deadline?: string;
};

export interface CollectModuleParam {
  /** collect module enum */
  type: CollectModuleEnum;
  /** 根据具体的Module类型，填写对应的数据 */
  data?: object | null;
}

export enum ReferenceModuleEnum {
  FollowerOnlyReferenceModule = "FollowerOnlyReferenceModule",
  NULL = "NullReferenceModule",
}

export interface ReferenceModule {
  type: ReferenceModuleEnum;
  /** You only have to enter one   */
  init?: object | null;
}

export interface ReferenceModuleParam {
  type: ReferenceModuleEnum;
  /** 根据具体的Module类型，填写对应的数据 */
  data?: object | null;
}

/** follow module enum */
export enum FollowModuleEnum {
  FeeFollowModule = "FeeFollowModule",
  ProfileFollowModule = "ProfileFollowModule",
  RevertFollowModule = "RevertFollowModule",
  NULL = "NullFollowModule",
}

export interface FollowModule {
  /** follow module enum */
  type: FollowModuleEnum;
  /** You only have to enter one  */
  init?: {
    FeeFollowModule?: FeeFollowModule;
  } | null;
}

export interface FeeFollowModule {
  amount?: Amount;
  /** The follow module recipient address */
  recipient?: string;
}

export interface FollowModuleParam {
  /** follow module enum */
  type: FollowModuleEnum;
  /** You only have to enter one  */
  data?: object | null;
}

export interface Broadcast {
  tx_hash: string;
}

export type BroadcastResponse = CommonResponse & {
  obj: Broadcast;
};

export interface BroadcastRequest {
  id: string;
  signature: string;
}

export interface TypeData {
  domain: object;
  types: object;
  value: object;
  // todo 暂时写为any 脚本导出的类型有误
  message?: any;
  primaryType?: string;
}

export type TypeDataResponse = CommonResponse & {
  obj: {
    type_data: TypeData;
    broadcast_id: string;
  };
};

export interface AddReactionRequest {
  /**
   * user id
   * @format string
   */
  user_id?: string;
  target_user_id?: string;
  /**
   * activity id
   * @format string
   */
  target_activity_id?: string;
  /**
   * The metadata uploaded somewhere passing in the url to reach it
   * @format uri
   */
  content_uri?: string;
  /** reaction kind */
  kind?: ReactionKindEnum;
  collect_module?: CollectModule;
  reference_module?: ReferenceModule;
  collect_module_param: CollectModuleParam;
  reference_module_param: ReferenceModuleParam;
}

/** reaction kind */
export enum ReactionKindEnum {
  COMMENT = "COMMENT",
  REPLY = "REPLY",
  LIKE = "LIKE",
  SHARE = "SHARE",
  FAVORITE = "FAVORITE",
  UPVOTE = "UPVOTE",
  DONWVOTE = "DONWVOTE",
}

/** reaction request */
export interface ListReactionRequest {
  /** reaction kind */
  kind?: ReactionKindEnum;
  /** reaction ranking type */
  ranking?: ReactionRankingEnum;
  /**
   * @min 1
   * @max 20
   */
  limit?: number;
  next_token?: string;
  /** @example false */
  with_activity?: boolean;
}

export type ReactionPaginationResponse = CommonResponse & {
  obj?: {
    /**
     * limit
     * @format int32
     * @min 1
     * @max 20
     */
    limit?: number;
    /**
     * total
     * @format int32
     */
    total?: number;
    /** nextToken */
    next_token?: string;
    rows?: Reaction[];
  };
};

/** reaction ranking type */
export enum ReactionRankingEnum {
  TIME = "TIME",
  HOT = "HOT",
  TOP = "TOP",
}

export interface Reaction {
  /** user id */
  user_id?: string;
  /** activity id */
  activity_id?: string;
  /**
   * The metadata uploaded somewhere passing in the url to reach it
   * @format uri
   */
  content_uri?: string;
  /** reaction kind */
  kind?: ReactionKindEnum;
  collect_module?: CollectModule;
  reference_module?: ReferenceModule;
}

export type ReactionResponse = CommonResponse & {
  obj?: Reaction;
};

/** query relation */
export interface ListRelationRequest {
  /** relation enum */
  relation?: RelationEnum;
  /** @default false */
  mutual?: boolean;
  /**
   * @min 1
   * @max 20
   */
  limit?: number;
  next_token?: string;
  /** @maxItems 20 */
  include_target_ids?: string[];
  /** @maxItems 5 */
  exclude_target_ids?: string[];
}

/** relation enum */
export enum RelationEnum {
  JOIN = "JOIN",
  FOLLOW = "FOLLOW",
  BLOCK = "BLOCK",
}

/** follow user */
export interface FollowRequest {
  /** @format string */
  id?: string;
  /**
   * user or community
   * @format string
   */
  type?: string;
  /** follow module redeem parameter */
  follow_module_param?: FollowModuleParam;
}

export interface FollowManyRequest {
  /** follow request */
  list?: FollowRequest[];
}

export type QueryParamsType = Record<string | number, any>;
export type ResponseFormat = keyof Omit<Body, "body" | "bodyUsed">;

export interface FullRequestParams extends Omit<RequestInit, "body"> {
  /** set parameter to `true` for call `securityWorker` for this request */
  secure?: boolean;
  /** request path */
  path: string;
  /** content type of request body */
  type?: ContentType;
  /** query params */
  query?: QueryParamsType;
  /** format of response (i.e. response.json() -> format: "json") */
  format?: ResponseFormat;
  /** request body */
  body?: unknown;
  /** base url */
  baseUrl?: string;
  /** request cancellation token */
  cancelToken?: CancelToken;
}

export type RequestParams = Omit<FullRequestParams, "body" | "method" | "query" | "path">;

export interface ApiConfig<SecurityDataType = unknown> {
  baseUrl?: string;
  baseApiParams?: Omit<RequestParams, "baseUrl" | "cancelToken" | "signal">;
  securityWorker?: (securityData: SecurityDataType | null) => Promise<RequestParams | void> | RequestParams | void;
  customFetch?: typeof fetch;
}

export interface HttpResponse<D extends unknown, E extends unknown = unknown> extends Response {
  obj: any;
  data: D;
  error: E;
}

type CancelToken = Symbol | string | number;

export enum ContentType {
  Json = "application/json",
  FormData = "multipart/form-data",
  UrlEncoded = "application/x-www-form-urlencoded",
  Text = "text/plain",
}

export class HttpClient<SecurityDataType = unknown> {
  public baseUrl: string = "https://{region}-{environment}.opensocial.{domain}/v2/";
  private securityData: SecurityDataType | null = null;
  private securityWorker?: ApiConfig<SecurityDataType>["securityWorker"];
  private abortControllers = new Map<CancelToken, AbortController>();
  private customFetch = (...fetchParams: Parameters<typeof fetch>) => fetch(...fetchParams);

  private baseApiParams: RequestParams = {
    credentials: "same-origin",
    headers: {},
    redirect: "follow",
    referrerPolicy: "no-referrer",
  };

  constructor(apiConfig: ApiConfig<SecurityDataType> = {}) {
    Object.assign(this, apiConfig);
  }

  public setSecurityData = (data: SecurityDataType | null) => {
    this.securityData = data;
  };

  protected encodeQueryParam(key: string, value: any) {
    const encodedKey = encodeURIComponent(key);
    return `${encodedKey}=${encodeURIComponent(typeof value === "number" ? value : `${value}`)}`;
  }

  protected addQueryParam(query: QueryParamsType, key: string) {
    return this.encodeQueryParam(key, query[key]);
  }

  protected addArrayQueryParam(query: QueryParamsType, key: string) {
    const value = query[key];
    return value.map((v: any) => this.encodeQueryParam(key, v)).join("&");
  }

  protected toQueryString(rawQuery?: QueryParamsType): string {
    const query = rawQuery || {};
    const keys = Object.keys(query).filter((key) => "undefined" !== typeof query[key]);
    return keys
      .map((key) => (Array.isArray(query[key]) ? this.addArrayQueryParam(query, key) : this.addQueryParam(query, key)))
      .join("&");
  }

  protected addQueryParams(rawQuery?: QueryParamsType): string {
    const queryString = this.toQueryString(rawQuery);
    return queryString ? `?${queryString}` : "";
  }

  private contentFormatters: Record<ContentType, (input: any) => any> = {
    [ContentType.Json]: (input: any) =>
      input !== null && (typeof input === "object" || typeof input === "string") ? JSON.stringify(input) : input,
    [ContentType.Text]: (input: any) => (input !== null && typeof input !== "string" ? JSON.stringify(input) : input),
    [ContentType.FormData]: (input: any) =>
      Object.keys(input || {}).reduce((formData, key) => {
        const property = input[key];
        formData.append(
          key,
          property instanceof Blob
            ? property
            : typeof property === "object" && property !== null
            ? JSON.stringify(property)
            : `${property}`,
        );
        return formData;
      }, new FormData()),
    [ContentType.UrlEncoded]: (input: any) => this.toQueryString(input),
  };

  protected mergeRequestParams(params1: RequestParams, params2?: RequestParams): RequestParams {
    return {
      ...this.baseApiParams,
      ...params1,
      ...(params2 || {}),
      headers: {
        ...(this.baseApiParams.headers || {}),
        ...(params1.headers || {}),
        ...((params2 && params2.headers) || {}),
      },
    };
  }

  protected createAbortSignal = (cancelToken: CancelToken): AbortSignal | undefined => {
    if (this.abortControllers.has(cancelToken)) {
      const abortController = this.abortControllers.get(cancelToken);
      if (abortController) {
        return abortController.signal;
      }
      return void 0;
    }

    const abortController = new AbortController();
    this.abortControllers.set(cancelToken, abortController);
    return abortController.signal;
  };

  public abortRequest = (cancelToken: CancelToken) => {
    const abortController = this.abortControllers.get(cancelToken);

    if (abortController) {
      abortController.abort();
      this.abortControllers.delete(cancelToken);
    }
  };

  public request = async <T = any, E = any>({
    body,
    secure,
    path,
    type,
    query,
    format,
    baseUrl,
    cancelToken,
    ...params
  }: FullRequestParams): Promise<HttpResponse<T, E>> => {
    const secureParams =
      ((typeof secure === "boolean" ? secure : this.baseApiParams.secure) &&
        this.securityWorker &&
        (await this.securityWorker(this.securityData))) ||
      {};
    const requestParams = this.mergeRequestParams(params, secureParams);
    console.log(params,secureParams,'sdsdds')
    const queryString = query && this.toQueryString(query);
    const payloadFormatter = this.contentFormatters[type || ContentType.Json];
    const responseFormat = format || requestParams.format;

    return this.customFetch(`${baseUrl || this.baseUrl || ""}${path}${queryString ? `?${queryString}` : ""}`, {
      ...requestParams,
      headers: {
        ...(requestParams.headers || {}),
        ...(type && type !== ContentType.FormData ? { "Content-Type": type } : {}),
      },
      signal: cancelToken ? this.createAbortSignal(cancelToken) : requestParams.signal,
      body: typeof body === "undefined" || body === null ? null : payloadFormatter(body),
    }).then(async (response) => {
      console.log(response,'responseresponse',responseFormat)
      const r = response as HttpResponse<T, E>;
      r.data = null as unknown as T;
      r.error = null as unknown as E;

      const data = !responseFormat
        ? r
        : await response[responseFormat]()
            .then((data) => {
              if (r.ok) {
                r.data = data;
              } else {
                r.error = data;
              }
              return r;
            })
            .catch((e) => {
              r.error = e;
              return r;
            });

      if (cancelToken) {
        this.abortControllers.delete(cancelToken);
      }

      if (!response.ok) throw data;
      return data;
    });
  };
}

/**
 * @title Open Social API
 * @version 2.0.0
 * @baseUrl https://{region}-{environment}.opensocial.{domain}/v2/
 *
 * The API for Open Social Project
 */
export class Api<SecurityDataType extends unknown> extends HttpClient<SecurityDataType> {
  delete(userId: string) {
    throw new Error('Method not implemented.');
  }
  auth = {
    /**
     * @description get challenge by address
     *
     * @tags Authentication
     * @name GetChallenge
     * @summary /auth/challenge
     * @request GET:/auth/challenge
     * @secure
     */
    getChallenge: (
      query: {
        /**
         * wallet address
         * @example "0xD8805CEcaD06E93Fc0976Ac8d094beeEA269ae37"
         */
        address: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<ChallengeResponse, void>({
        path: `/auth/challenge`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description login by challenge
     *
     * @tags Authentication
     * @name Authenticate
     * @summary /auth/login
     * @request POST:/auth/login
     * @secure
     */
    authenticate: (data: AuthRequest, params: RequestParams = {}) =>
      this.request<TokenResponse, void>({
        path: `/auth/login`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description verify access token
     *
     * @tags Authentication
     * @name Verify
     * @summary /auth/verify
     * @request POST:/auth/verify
     * @secure
     */
    verify: (
      query: {
        /**
         * verify access token
         * @example ""
         */
        access_token: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<CommonResponse, void>({
        path: `/auth/verify`,
        method: "POST",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description refresh access token
     *
     * @tags Authentication
     * @name RefreshToken
     * @summary /auth/refresh
     * @request POST:/auth/refresh
     * @secure
     */
    refreshToken: (
      query: {
        /**
         * refresh access token
         * @example ""
         */
        refresh_token: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<TokenResponse, void>({
        path: `/auth/refresh`,
        method: "POST",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),
  };
  activities = {
    /**
     * No description
     *
     * @tags Activity
     * @name GetActivities
     * @summary /activities
     * @request GET:/activities
     * @secure
     */
    getActivities: (
      query: GetActivitiesRequest,
      params: RequestParams = {},
    ) =>
      this.request<ActivityPaginationResponse, any>({
        path: `/activities`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description 该接口同时支持链下发帖和dispatcher模式发帖，发布帖子/评论/转发前都需要进行内容元数据的构建，构建的内容标准需要遵循 Metadata structure的定义，最终将json文件上传到**IPFS**作为content_uri的内容，为了保持统一，不需要上链同样需要进行这一步操作
     *
     * @tags Activity
     * @name AddActivity
     * @summary /activities
     * @request POST:/activities
     * @secure
     */
    addActivity: (data: AddActivityRequest, params: RequestParams = {}) =>
      this.request<TransactionResponse, any>({
        path: `/activities`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description get activity by id
     *
     * @tags Activity
     * @name GetActivity
     * @summary /activities/{activity_id}
     * @request GET:/activities/{activity_id}
     * @secure
     */
    getActivity: (activityId: string, params: RequestParams = {}) =>
      this.request<ActivityResponse, any>({
        path: `/activities/${activityId}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags Activity
     * @name StopActivity
     * @summary /activities/{activity_id}
     * @request DELETE:/activities/{activity_id}
     * @secure
     */
    stopActivity: (activityId: string, params: RequestParams = {}) =>
      this.request<CommonResponse, any>({
        path: `/activities/${activityId}`,
        method: "DELETE",
        secure: true,
        format: "json",
        ...params,
      }),
  };
  users = {
    /**
     * @description create user
     *
     * @tags User
     * @name CreateUser
     * @summary /users
     * @request POST:/users
     * @secure
     */
    createUser: (
      data: {
        follow_module?: FollowModule;
        /**
         * The follow NFT URI is the NFT metadata your followers will mint when they follow you
         * @format uri
         */
        follow_nft_uri?: string;
        handle?: string;
        bio?: string;
        avatar?: string;
        cover_picture?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<TransactionResponse, any>({
        path: `/users`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name GetUsers
     * @summary /users
     * @request GET:/users
     * @secure
     */
    getUsers: (
      query?: GetUserRequest,
      params: RequestParams = {},
    ) =>
      this.request<UserPaginationResponse, any>({
        path: `/users`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description get user by id
     *
     * @tags User
     * @name GetUser
     * @summary /users/{id}
     * @request GET:/users/{id}
     * @secure
     */
    getUser: (id: string, params: RequestParams = {}) =>
      this.request<UserResponse, any>({
        path: `/users/${id}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description modify user
     *
     * @tags User
     * @name UpdateUser
     * @summary /users/{id}
     * @request PUT:/users/{id}
     * @secure
     */
    updateUser: (
      id: string,
      query?: {
        /**
         * update key, eg. userImage/avatar
         * @example ""
         */
        key?: string;
        /**
         * update value
         * @example ""
         */
        value?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<TransactionResponse, any>({
        path: `/users/${id}`,
        method: "PUT",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description close user
     *
     * @tags User
     * @name DeleteProfile
     * @summary /users/{id}
     * @request DELETE:/users/{id}
     * @secure
     */
    deleteProfile: (id: string, params: RequestParams = {}) =>
      this.request<TransactionResponse, any>({
        path: `/users/${id}`,
        method: "DELETE",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description set user dispatcher
     *
     * @tags User
     * @name SetDispatcher
     * @summary /users/{id}/dispatcher
     * @request POST:/users/{id}/dispatcher
     * @secure
     */
    setDispatcher: (
      id: string,
      query?: {
        /** dispatcher info */
        dispatcher?: Dispatcher;
      },
      params: RequestParams = {},
    ) =>
      this.request<TransactionResponse, any>({
        path: `/users/${id}/dispatcher`,
        method: "POST",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description update user dispatcher
     *
     * @tags User
     * @name UpdateDispatcher
     * @summary /users/{id}/dispatcher
     * @request PUT:/users/{id}/dispatcher
     * @secure
     */
    updateDispatcher: (
      id: string,
      query?: {
        /** flag for enable */
        enable?: boolean;
      },
      params: RequestParams = {},
    ) =>
      this.request<TransactionResponse, any>({
        path: `/users/${id}/dispatcher`,
        method: "PUT",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),
  };
  reaction = {
    /**
     * @description add reaction
     *
     * @tags Reaction
     * @name AddReaction
     * @summary /reaction
     * @request POST:/reaction
     * @secure
     */
    addReaction: (data: AddReactionRequest, params: RequestParams = {}) =>
      this.request<TransactionResponse, any>({
        path: `/reaction`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description get reaction by id
     *
     * @tags Reaction
     * @name GetReaction
     * @summary /reaction/{id}
     * @request GET:/reaction/{id}
     * @secure
     */
    getReaction: (id: string, params: RequestParams = {}) =>
      this.request<ReactionResponse, any>({
        path: `/reaction/${id}`,
        method: "GET",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * @description delete reaction by id
     *
     * @tags Reaction
     * @name DeleteReaction
     * @summary /reaction/{id}
     * @request DELETE:/reaction/{id}
     * @secure
     */
    deleteReaction: (id: string, params: RequestParams = {}) =>
      this.request<CommonResponse, any>({
        path: `/reaction/${id}`,
        method: "DELETE",
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags Reaction
     * @name GetReactions
     * @summary /reaction/{lookup_attr}/{lookup_value}/{kind}
     * @request GET:/reaction/{lookup_attr}/{lookup_value}/{kind}
     * @secure
     */
    getReactions: (
      lookupAttr: string,
      lookupValue: string,
      kind: ReactionKindEnum,
      query: {
        /** reaction request */
        listReactionRequest: ListReactionRequest;
      },
      params: RequestParams = {},
    ) =>
      this.request<ReactionPaginationResponse, any>({
        path: `/reaction/${lookupAttr}/${lookupValue}/${kind}`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags Reaction
     * @name DeleteReactionByKind
     * @summary /reaction/{lookup_attr}/{lookup_value}/{kind}
     * @request DELETE:/reaction/{lookup_attr}/{lookup_value}/{kind}
     * @secure
     */
    deleteReactionByKind: (
      lookupAttr: string,
      lookupValue: string,
      kind: ReactionKindEnum,
      params: RequestParams = {},
    ) =>
      this.request<CommonResponse, any>({
        path: `/reaction/${lookupAttr}/${lookupValue}/${kind}`,
        method: "DELETE",
        secure: true,
        format: "json",
        ...params,
      }),
  };
  feed = {
    /**
     * No description
     *
     * @tags Relation
     * @name ListFollowers
     * @summary /feed/{feed_slug}/{user_id}/follower
     * @request GET:/feed/{feed_slug}/{user_id}/follower
     * @secure
     */
    listFollowers: (
      feedSlug: string,
      userId: string,
      query?: ListRelationRequest,
      params: RequestParams = {},
    ) =>
      this.request<UserPaginationResponse, any>({
        path: `/feed/${feedSlug}/${userId}/follower`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags Relation
     * @name AddRelation
     * @summary /feed/{feed_slug}/{user_id}/following
     * @request POST:/feed/{feed_slug}/{user_id}/following
     * @secure
     */
    addRelation: (feedSlug: string, userId: string, data: FollowRequest, params: RequestParams = {}) =>
      this.request<TransactionResponse, any>({
        path: `/feed/${feedSlug}/${userId}/following`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags Relation
     * @name GetFollowing
     * @summary /feed/{feed_slug}/{user_id}/following
     * @request GET:/feed/{feed_slug}/{user_id}/following
     * @secure
     */
    getFollowing: (
      feedSlug: string,
      userId: string,
      query?: {
        /**
         * wallet address
         * @example ""
         */
        address?: string;
        /**
         * limit
         * @example ""
         */
        limit?: number;
        /**
         * next_token
         * @example ""
         */
        next_token?: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<UserPaginationResponse, any>({
        path: `/feed/${feedSlug}/${userId}/following`,
        method: "GET",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),
  };
  followMany = {
    /**
     * No description
     *
     * @tags Relation
     * @name AddRelations
     * @summary /follow_many
     * @request POST:/follow_many
     * @secure
     */
    addRelations: (
      query?: {
        /** request info */
        followManyRequest?: FollowManyRequest;
      },
      params: RequestParams = {},
    ) =>
      this.request<TransactionResponse, any>({
        path: `/follow_many`,
        method: "POST",
        query: query,
        secure: true,
        format: "json",
        ...params,
      }),
  };
  broadcast = {
    /**
     * @description ## 根据签名发送交易，测试使用
     *
     * @tags Broadcast
     * @name BroadcastCreate
     * @summary /broadcast
     * @request POST:/broadcast
     * @secure
     */
    broadcastCreate: (data: BroadcastRequest, params: RequestParams = {}) =>
      this.request<BroadcastResponse, any>({
        path: `/broadcast`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  typedata = {
    /**
     * No description
     *
     * @tags TypeData
     * @name ProfileDispatcherCreate
     * @summary /typedata/profile/dispatcher
     * @request POST:/typedata/profile/dispatcher
     * @secure
     */
    profileDispatcherCreate: (
      data: {
        dispatcher: string;
        profile_id: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<TypeDataResponse, any>({
        path: `/typedata/profile/dispatcher`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags TypeData
     * @name ActivityCreate
     * @summary /typedata/activity
     * @request POST:/typedata/activity
     * @secure
     */
    activityCreate: (data: AddActivityRequest, params: RequestParams = {}) =>
      this.request<TypeDataResponse, any>({
        path: `/typedata/activity`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags TypeData
     * @name RelationCreate
     * @summary /typedata/relation
     * @request POST:/typedata/relation
     * @secure
     */
    relationCreate: (data: FollowRequest, params: RequestParams = {}) =>
      this.request<TypeDataResponse, any>({
        path: `/typedata/relation`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * No description
     *
     * @tags TypeData
     * @name ReactionCreate
     * @summary /typedata/reaction
     * @request POST:/typedata/reaction
     * @secure
     */
    reactionCreate: (data: AddReactionRequest, params: RequestParams = {}) =>
      this.request<TypeDataResponse, any>({
        path: `/typedata/reaction`,
        method: "POST",
        body: data,
        secure: true,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
}
