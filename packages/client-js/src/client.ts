import { Activity } from './activity'
import { Relation } from './relation'
import { User } from './user'
import { Reaction } from './reaction'
import { TypeData } from './typeData'
import { Broadcast } from './broadcast'
import { Auth } from './auth'
import { Api } from './rest_api_generated'
import { IPFS } from './ipfs'
import Web3 from 'web3'
import { AbiItem } from 'web3-utils'

const CONTRACH_ADDRESS = '0xa513E6E4b8f2a923D98304ec87F64353C4D5C853'

export type UR = Record<string, unknown>;
export type DefaultGenerics={
  activityType: UR;
  childReactionType: UR;
  collectionType: UR;
  personalizationType: UR;
  reactionType: UR;
  userType: UR;
}

export type ClientOptions={
  urlOverride?: Record<string, string>,
  timeout?: number,
  browser?: boolean,
  version?: string,
  cache?: boolean,
  cacheProvider?: any
}

export type ContractConfig={
  abi?: AbiItem[],
  gasPrice?: string,
}

export class Client<StreamFeedGenerics extends DefaultGenerics = DefaultGenerics>{
  baseUrl: string;
  apiKey: string;
  appId?: string;
  apiSecret: string | null;
  userId?: string;
  version: string;
  options: ClientOptions
  enableCache: boolean
  isBrowser: boolean
  // 不确定
  cache: any
  web3: Web3
  address: string
  request: Api<{}>
  ipfs: IPFS
  activity: Activity<StreamFeedGenerics>
  relation: Relation<StreamFeedGenerics>
  user: User<StreamFeedGenerics>
  reaction: Reaction<StreamFeedGenerics>
  auth: Auth<StreamFeedGenerics>
  typedata: TypeData<StreamFeedGenerics>
  broadcast: Broadcast<StreamFeedGenerics>
  contract: any
  contractConfig: ContractConfig
  // reactions: StreamReaction<StreamFeedGenerics>;
  constructor(
    apiKey: string, 
    apiSecretOrToken: string | null, 
    appId?: string, 
    options: ClientOptions = {},
    contractConfig?: ContractConfig
    ) {
    this.apiKey = apiKey;
    this.apiSecret = apiSecretOrToken
    
    this.appId = appId;
    this.options = options;
    this.version = this.options.version || 'v1.0';
    this.enableCache = this.options.cache || true
    this.baseUrl = this.getBaseUrl();
    this.isBrowser = typeof location!== 'undefined' && this.options.browser ? true : false
    this.cache = typeof localStorage !== 'undefined' ? localStorage :  this.options.cacheProvider
    this.contractConfig= contractConfig
    const { abi,gasPrice='20000000000' } = contractConfig || {}
    if(this.isBrowser){
      if(Web3?.givenProvider){
        this.web3 = new Web3(Web3.givenProvider || "ws://localhost:8546");
        this.web3.eth.requestAccounts().then((accounts: string[])=>{
          this.address = accounts[0]
          this.cache.setItem('address', this.address)
          if(!abi) return
          this.contract = new this.web3.eth.Contract(abi, CONTRACH_ADDRESS, {
            from: accounts[0], // 默认交易发送地址
            gasPrice // 以 wei 为单位的默认 gas 价格，当前价格为 20 gwei
          });
          if(!this.address){
            throw new Error('Please use a valid wallet address!')
          }
        });
      } else {
        throw new Error('Please use a browser wallet')
      }
    }

    this.request = new Api({
      baseUrl: this.baseUrl,
      securityWorker: (secureData) => secureData
    })
    this.ipfs = new IPFS(this)
    this.activity = new Activity<StreamFeedGenerics>(this)
    this.relation = new Relation<StreamFeedGenerics>(this);
    this.user = new User<StreamFeedGenerics>(this);
    this.reaction = new Reaction<StreamFeedGenerics>(this);
    this.auth = new Auth<StreamFeedGenerics>(this);
    this.typedata = new TypeData<StreamFeedGenerics>(this);
    this.broadcast = new Broadcast<StreamFeedGenerics>(this);
    console.log(this.address,'ppppppppp')
  }
  getBaseUrl(serviceName?: string) {
    if (!serviceName) serviceName = 'api';

    if (this.options.urlOverride && this.options.urlOverride[serviceName]) return this.options.urlOverride[serviceName];

    return this.baseUrl;
  }

  initAbi(abi: AbiItem[],gasPrice='20000000000',from: string){
    this.contract = new this.web3.eth.Contract(abi, CONTRACH_ADDRESS, {
      from, // 默认交易发送地址
      gasPrice
    });
  }



}
